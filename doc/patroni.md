## Patroni setup

This PoC uses Docker to setup a clean cluster of Patroni based on the image of PostgreSQL 10.

### Build

To build the Dockerfile:

```
docker-compose -f docker-compose-patroni.yml build
```

To run:

```
docker-compose -f docker-compose-patroni.yml up --scale consul-bootstrap=1 --scale consul-server=2 --scale pg=4
```


### Run

To start the cluster of 3 consul and 3 Patroni containers, we use docker-compose,
the filename is patroni-compose.yml and the command to run the compose yml is:

docker-compose -f patroni-compose.yml up -d

The compose stack contains the following services definitions:

```
consul-1
consul-2
consul-3
pg-1
pg-2
pg-3
```

### Check

We can later check the log output of each container

```
docker-compose -f patroni-compose.yml logs <container>
```

Some example output from Patroni:

```
pg-2 container:
pg-2_1      | 2018-08-02 12:47:47,466 INFO: Lock owner: db2; I am db2
pg-2_1      | 2018-08-02 12:47:47,468 INFO: no action.  i am the leader with the lock
pg-3 container:
pg-3_1      | 2018-08-02 12:48:08,638 INFO: Lock owner: db2; I am db3
pg-3_1      | 2018-08-02 12:48:08,638 INFO: does not have lock
pg-3_1      | 2018-08-02 12:48:08,642 INFO: no action.  i am a secondary and i am following a leader
```


We can now connect to each PG container and test that replication is working.
