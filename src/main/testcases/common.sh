set -eu

BASE_DIR="$(dirname "$0")"
REPMGR_VERSION=3.3

while [ "$#" -gt 1 ]
do
  case "$1" in
    --repmgr-version)
      shift
      REPMGR_VERSION="$1"
      shift
      ;;
    *)
      >&2 echo "Unknown parameter $1"
      exit 1
      ;;
  esac
done

function configure-by-repmgr-version() {
  if [ "$REPMGR_VERSION" == "3.3" ]
  then
    export repmgr_version=3.3
    export repmgr_table=repmgr_main.repl_nodes
  else
    export repmgr_version=4.1.0
    export repmgr_table=repmgr.nodes
  fi
}

function wait-repmgr-active(){
  while ! docker exec "repmgrpoc_$1_1" /usr/local/pgsql/bin/psql -U postgres -d repmgr \
    -A -t -c 'select count(1) from '"$repmgr_table"' where active' 2>/dev/null|grep -q "^$2$"
  do
    sleep 0.3
  done
}

function wait-pgbouncer-monitor-running(){
  while ! docker exec "repmgrpoc_pgb-1_1" /usr/local/pgsql/bin/psql -U postgres -h localhost -p 6432 \
    -A -t -c 'select 1 from test limit 1' 2>/dev/null|grep -q "^1$"
  do
    sleep 0.3
  done
}

function stop-postgres(){
  docker exec "repmgrpoc_$1_1" bash /scripts/postgresql/stop-postgres.sh 5432
}

function repmgr-standby-clone-and-follow(){
  docker exec "repmgrpoc_$1_1" bash /scripts/repmgr/standby-clone.sh 5432 "$2"
  docker exec "repmgrpoc_$1_1" bash /scripts/repmgr/standby-follow.sh 5432
}

function stop-repmgrd(){
  docker exec "repmgrpoc_$1_1" bash /scripts/repmgr/stop-repmgrd.sh 5432
}

function check-root(){
  if [ "$(id -u)" != "0" ]
  then
    >&2 echo "This testcase must be run as root"
    return 1
  fi
}

function monitor-pgbouncer(){
  exec java -jar "$BASE_DIR"/../../../target/dist/lib/failover-bot-*.jar monitor-primary-instance --target pgb-1 --jdbc-port 6432 "$@"
}

function network-failure(){
  exec java -jar "$BASE_DIR"/../../../target/dist/lib/failover-bot-*.jar testcase-network-failure "$@"
}

function is-master(){
  (docker exec "repmgrpoc_$1_1" /usr/local/pgsql/bin/psql -U postgres \
    -A -t -c 'select pg_is_in_recovery()' 2>/dev/null||true)|grep -q "^f$"
}

function current-master(){
  count=0
  for container in pg-1 pg-2 pg-3 pg-4
  do
    if is-master "$container"
    then
      echo "$container"
    fi
  done
}

function monitor-and-check-failover-from(){
  last_master=("$1")
  current_master=("${last_master[@]}")
  echo "Current master is ${last_master[*]}"
  start="$(date +%s)"
  timeout=true
  while [ "$(($(date +%s)-start))" -lt 240 ]
  do
    current_master=($(current-master))
    if [ "${last_master[*]}" != "${current_master[*]}" ]
    then
      timeout=false
      break
    fi
    sleep 1
  done
  
  if $timeout
  then
    >&2 echo "Nothing changed after 4min timeout"
    return 1
  fi
  
  echo "Master changed from ${last_master[*]} to ${current_master[*]}"
  
  while [ "$(($(date +%s)-start))" -lt 240 ]
  do
    if [ "${#current_master[@]}" -gt "0" ]
    then
      timeout=false
      break
    fi
    
    sleep 1
    
    current_master=($(current-master))
  done
  
  if $timeout
  then
    >&2 echo "No master after 4min timeout"
    return 1
  fi
  
  if [ "${#current_master[@]}" -gt "1" ]
  then
    >&2 echo "Found ${#current_master[@]} masters: ${current_master[*]}"
    return 1
  fi
  
  echo "New master is ${current_master[*]}"
  return 0
}

function monitor-and-check-master(){
  current_master=($(current-master))
  start="$(date +%s)"
  
  while [ "$(($(date +%s)-start))" -lt 240 ]
  do
    current_master=($(current-master))
    
    if [ "${#current_master[@]}" -ne "1" ]
    then
      break
    fi
    
    sleep 1
  done
  
  if [ "${#current_master[@]}" -gt "1" ]
  then
    >&2 echo "Found ${#current_master[@]} masters: ${current_master[*]}"
    return 1
  fi
  
  if [ "${#current_master[@]}" -eq "0" ]
  then
    >&2 echo "Master not present"
    return 1
  fi
  
  echo "Master is still ${current_master[*]}"
  return 0
}