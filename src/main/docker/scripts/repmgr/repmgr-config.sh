#!/bin/bash

set -e

PGPORT="$1"
PGDATA="/var/lib/postgresql/data-$PGPORT"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift

if [ "$REPMGR_VERSION" == "3.3" ]
then
  echo "node=$ID
node_name=$(hostname)
conninfo='host=$(hostname) port=$PGPORT user=postgres dbname=repmgr'
cluster=main
pg_bindir=/usr/local/pgsql/bin
promote_command='/usr/local/pgsql/bin/repmgr standby promote -f \"$REPMGRCONF\" --log-to-file'
follow_command='/usr/local/pgsql/bin/repmgr standby follow -f \"$REPMGRCONF\" --log-to-file'
failover=automatic
event_notifications=repmgrd_failover_promote
event_notification_command='bash /scripts/repmgr/repmgrd-event-handler.sh $PGPORT %n %e %s \"%t\" \"%d\"'
monitor_interval_secs=2
reconnect_attempts=6
reconnect_interval=5
retry_promote_interval_secs=300
witness_repl_nodes_sync_interval_secs=15
loglevel=DEBUG
logfile=/var/log/repmgr-$PGPORT.log"
else
  echo "node_id=$ID
node_name=$(hostname)
conninfo='host=$(hostname) port=$PGPORT user=postgres dbname=repmgr'
data_directory=$PGDATA
pg_bindir=/usr/local/pgsql/bin
promote_command='/usr/local/pgsql/bin/repmgr standby promote -f \"$REPMGRCONF\" --log-to-file'
follow_command='/usr/local/pgsql/bin/repmgr standby follow -f \"$REPMGRCONF\" --log-to-file'
failover=automatic
event_notifications=repmgrd_failover_promote
event_notification_command='bash /scripts/repmgr/repmgrd-event-handler.sh $PGPORT %n %e %s \"%t\" \"%d\"'
monitor_interval_secs=2
primary_notification_timeout=60
reconnect_attempts=6
reconnect_interval=5
promote_check_timeout=300
witness_sync_interval=15
log_level=DEBUG
log_file=/var/log/repmgr-$PGPORT.log"
fi