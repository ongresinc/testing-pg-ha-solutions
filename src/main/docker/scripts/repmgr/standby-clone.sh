#!/bin/bash

set -e

PGPORT="$1"
PGDATA="/var/lib/postgresql/data-$PGPORT"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift
master_host="$1"
shift

rm -Rf "$PGDATA"
mkdir -p "$PGDATA"
chown postgres:postgres "$PGDATA"

export ID=$(hostname|sed 's/pg-/1/')
bash /scripts/repmgr/repmgr-config.sh "$PGPORT" > "$REPMGRCONF"
chown postgres:postgres "$REPMGRCONF"
su postgres -c "/usr/local/pgsql/bin/repmgr -f '$REPMGRCONF' -h '$master_host' -d repmgr -F standby clone"
  nohup sh > /dev/null 2>&1 << EOF &
    exec su postgres -c "/usr/local/pgsql/bin/pg_ctl start -D $PGDATA"
EOF
while ! /usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d template1 -c "SELECT 1" 1>/dev/null 2>&1; do sleep 0.3; done;
