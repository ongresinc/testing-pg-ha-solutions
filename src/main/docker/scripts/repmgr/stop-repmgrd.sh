#!/bin/bash

set -e

PGPORT="$1"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift

if [ -f /var/run/repmgrd-$PGPORT.pid ] && kill -0 "$(cat /var/run/repmgrd-$PGPORT.pid)" \
  && [ "$(ps --pid "$(cat /var/run/repmgrd-$PGPORT.pid)" -o stat|tail -n +2)" != "Z" ]
then
  kill "$(cat /var/run/repmgrd-$PGPORT.pid)"
  rm -f /var/run/repmgrd-$PGPORT.pid
fi
