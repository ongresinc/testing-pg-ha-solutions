#!/bin/bash

set -e

PGPORT="$1"
PGDATA="/var/lib/postgresql/data-$PGPORT"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift

export ID=$(hostname|sed 's/pg-/1/')

while ! /usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d repmgr -c "SELECT 1" 1>/dev/null 2>&1; do sleep 0.3; done;

bash /scripts/repmgr/repmgr-config.sh "$PGPORT" > "$REPMGRCONF"
chown postgres:postgres "$REPMGRCONF"
su postgres -c "/usr/local/pgsql/bin/repmgr -f '$REPMGRCONF' standby register -F"
