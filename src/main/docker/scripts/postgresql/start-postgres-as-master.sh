#!/bin/bash

set -e

PGPORT="$1"
PGDATA="/var/lib/postgresql/data-$PGPORT"

nohup sh > /var/log/prepare_backup-$PGPORT.log 2>&1 << EOF &
  echo "Preparing backup for instance $1"
  export PGPORT=$1
  export PGDATA=/var/lib/postgresql/data-$PGPORT

  mkdir -p /var/lib/postgresql/basebackup-$PGPORT
  while ! /usr/local/pgsql/bin/pg_basebackup -U postgres -h localhost -p $PGPORT -D /var/lib/postgresql/basebackup-$PGPORT -F t -x 2>/dev/null
  do
    sleep 0.3
  done
  while true
  do
    nc -q 0 -l -p 1$PGPORT < /var/lib/postgresql/basebackup-$PGPORT/base.tar
  done
EOF

echo "Starting postgres master instance on port $PGPORT"

rm -Rf $PGDATA || true

useradd postgres || true
mkdir -p $PGDATA
chown postgres:postgres $PGDATA

su postgres -c "/usr/local/pgsql/bin/initdb -D $PGDATA"

echo "Enabling standby"
echo "
wal_level = hot_standby
max_wal_senders = 10
wal_keep_segments = 10
hot_standby = on
archive_mode = on
archive_command = '/bin/true'" >> "$PGDATA/postgresql.conf"

echo "Enabling port and log override in postgresql.conf"
echo "
listen_addresses = '*'
port = $PGPORT
logging_collector = on
log_directory = '/var/log'
log_filename = postgres-$PGPORT.log
log_rotation_age = 0
log_rotation_size = 0" >> "$PGDATA/postgresql.conf"

echo "Enabling recovery"
echo "
recovery_target_timeline='latest'
standby_mode = on
primary_conninfo = 'host=${master_host} port=$PGPORT'" > "$PGDATA/recovery.done"
chown postgres:postgres $PGDATA/recovery.done
echo "host all postgres 0.0.0.0/0 trust" >> "$PGDATA/pg_hba.conf"
echo "host replication postgres 0.0.0.0/0 trust" >> "$PGDATA/pg_hba.conf"

echo "Enabling repmgr"
if [ "$REPMGR_VERSION" == "3.3" ]
then
  echo "shared_preload_libraries = 'repmgr_funcs'" >> "$PGDATA/postgresql.conf"
else
  echo "shared_preload_libraries = 'repmgr'" >> "$PGDATA/postgresql.conf"
fi
echo "local repmgr postgres trust" >> "$PGDATA/pg_hba.conf"
echo "host repmgr postgres 0.0.0.0/0 trust" >> "$PGDATA/pg_hba.conf"

chmod a+w /var/log

nohup sh > /dev/null 2>&1 << EOF &
  exec su postgres -c "/usr/local/pgsql/bin/pg_ctl start -D $PGDATA"
EOF

while [ ! -f /var/log/postgres-$PGPORT.log ]; do sleep 1; done

tail -f /var/log/postgres-*.log /var/log/prepare_backup-*.log &
echo $! > /var/run/tail.pid

while ! /usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d template1 -c "SELECT 1" 1>/dev/null 2>&1; do sleep 0.3; done;
echo "All postgres master are ready!";

kill $(cat /var/run/tail.pid)