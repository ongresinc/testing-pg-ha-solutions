#!/bin/bash

set -e
export port="$1"
export table="$2"

echo "writing some data to $table... "
/usr/local/pgsql/bin/psql -U postgres -p $port -d postgres -c "CREATE TABLE IF NOT EXISTS $table(i integer)"

for n in $(seq 1 4)
do
(
echo -n "INSERT INTO $table VALUES "
for i in $(seq 1 250)
do 
echo -n "(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1), 
		(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),"
done
echo -n "(1)"
) | /usr/local/pgsql/bin/psql -U postgres -p $port -d postgres &
done
wait
echo "writing some data...finished ($(/usr/local/pgsql/bin/psql -U postgres -p $port -d postgres -t -c "select count(1) from $table"))"
