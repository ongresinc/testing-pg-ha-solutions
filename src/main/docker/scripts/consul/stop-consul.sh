#!/bin/bash

set -e

killall consul 2>/dev/null || true

while consul info > /dev/null 2>&1; do sleep 0.3; done;