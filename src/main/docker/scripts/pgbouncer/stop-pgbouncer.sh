#!/bin/bash

set -e

PGBPORT="$1"
PGBDATA="/var/lib/pgbouncer/data-$PGBPORT"

if [ -f $PGBDATA/pgbouncer.pid ] && kill -0 "$(cat $PGBDATA/pgbouncer.pid)" \
  && [ "$(ps --pid "$(cat $PGBDATA/pgbouncer.pid)" -o stat|tail -n +2)" != "Z" ]
then
  kill "$(cat $PGBDATA/pgbouncer.pid)"
  rm -f $PGBDATA/pgbouncer.pid
fi
