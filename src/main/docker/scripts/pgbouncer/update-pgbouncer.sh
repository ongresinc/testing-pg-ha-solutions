#!/bin/bash

set -e

PGBPORT="$1"
PGHOST="$2"
PGPORT="$3"

PGBDATA="/var/lib/pgbouncer/data-$PGBPORT"

bash /scripts/pgbouncer/pgbouncer-config.sh "$PGBPORT" "$PGHOST" "$PGPORT" > "$PGBDATA/pgbouncer.ini"

if [ -f $PGBDATA/pgbouncer.pid ] && kill -0 "$(cat $PGBDATA/pgbouncer.pid)" \
  && [ "$(ps --pid "$(cat $PGBDATA/pgbouncer.pid)" -o stat|tail -n +2)" != "Z" ]
then
  kill -1 "$(cat $PGBDATA/pgbouncer.pid)"
else
  su postgres -c "pgbouncer -d '$PGBDATA/pgbouncer.ini'"
fi
