#!/bin/bash

set -e

PGBPORT="$1"
PGHOST="$2"
PGPORT="$3"

PGBDATA="/var/lib/pgbouncer/data-$PGBPORT"

echo "
[pgbouncer]
logfile = /var/log/pgbouncer-$PGBPORT.log
pidfile = $PGBDATA/pgbouncer.pid
listen_addr = 0.0.0.0
listen_port = $PGBPORT
unix_socket_dir = $PGBDATA/
auth_type = trust
auth_query = SELECT username, password FROM public.pg_shadow_lookup($1)
admin_users = postgres
stats_users = postgres
pool_mode = transaction
server_reset_query = DISCARD ALL
ignore_startup_parameters = extra_float_digits
application_name_add_host = 1
max_client_conn = 4096
default_pool_size = 100
min_pool_size = 0
reserve_pool_size = 5
reserve_pool_timeout = 3
server_round_robin = 0
server_idle_timeout = 65
dns_max_ttl = 15.0
dns_zone_check_period = 0
dns_nxdomain_ttl = 15.0
[databases]
postgres = host=$PGHOST port=$PGPORT auth_user=postgres
"