/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

import com.ongres.gitlab.bot.Main.SubCommand;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObject;

@Command(name = "testcase-network-failure", 
    description = "Run a test case that simulate a network failure")
public class TestcaseNetworkFailure extends SubCommand {
  
  @Option(names = {"--jdbc-url-template"},
      description = "The JDBC URL template.")
  protected String jdbcUrlTemplate = 
      "jdbc:postgresql://${address}:${port}/${database}?user=${user}&password=${password}";
  
  @Option(names = {"--jdbc-database"},
      description = "The JDBC database.")
  protected String jdbcDatabase = "postgres";
  
  @Option(names = {"--jdbc-user"},
      description = "The JDBC user.")
  protected String jdbcUser = "postgres";
  
  @Option(names = {"--jdbc-password"},
      description = "The JDBC password.")
  protected String jdbcPassword = "trustme";
  
  @Option(names = {"--failure-type"},
      description = "The netwrok failure type (node, primary, datacenter).")
  protected NetworkFailureType networkFailureType;
  
  @Option(names = {"--failure-target"},
      description = "The netwrok failure target (Depending on --failure-type: specify container name when 'node';"
          + "specify a comma separated list of container name when 'nodes';"
          + " specify a datacenter name when 'datacenter'; ignored when 'primary').")
  protected String networkFailureTarget;
  
  @Option(names = {"--failure-duration"},
      description = "The netwrok failure duration in seconds.")
  protected Integer networkFailureDuration;
  
  public enum NetworkFailureType {
    node(180),
    nodes(180),
    primary(180),
    datacenter(420);
    
    private final int secondsToSleep;
    
    private NetworkFailureType(int secondsToSleep) {
      this.secondsToSleep = secondsToSleep;
    }
    
    public int getSecondsToSleep() {
      return secondsToSleep;
    }
  }
  
  @Override
  public void run() {
    Random random = new Random();
    
    NetworkFailureType networkFailureType = 
        this.networkFailureType != null ? this.networkFailureType
        : NetworkFailureType.values()[random.nextInt(NetworkFailureType.values().length)];
    
    List<JsonObject> containers = Json.createReader(new StringReader(
        parent.waitProcessAndGet("docker inspect " 
            + parent.waitProcessAndGet("docker ps -q").replace("\n", " "))))
        .read()
        .asJsonArray()
        .stream()
        .map(entry -> entry.asJsonObject())
        .filter(entry -> entry
            .getJsonObject("NetworkSettings")
            .getJsonObject("Networks")
            .containsKey("repmgrpoc_network"))
        .collect(Collectors.toList());
    
    Predicate<JsonObject> filter;
    switch (networkFailureType) {
      case datacenter:
      {
        String dataCenter = this.networkFailureTarget != null 
            ? this.networkFailureTarget : new String[] { "snc", "dub" }[random.nextInt(2)];
        System.out.println("Network isolation for data center " + dataCenter);
        filter = entry -> entry
            .getString("Name").matches("/repmgrpoc_.*-" + dataCenter + "-.*");
      }
        break;
      case nodes:
      {
        List<String> containerNames = this.networkFailureTarget != null 
            ? Arrays.asList(this.networkFailureTarget.split(",")).stream()
                .map(networkFailureTarget -> "/repmgrpoc_" + networkFailureTarget + "_1")
                .collect(Collectors.toList()) 
            : containers.stream()
              .sorted((o1, o2) -> random.nextInt())
              .limit(random.nextInt() % containers.size())
              .map(entry -> entry.getString("Name"))
              .collect(Collectors.toList());
        System.out.println("Network isolation for nodes " + containerNames.stream()
          .map(containerName -> containerName.substring("/repmgrpoc_".length()))
          .map(containerName -> containerName.substring(0, containerName.length() - "_1".length()))
          .collect(Collectors.toList()));
        filter = entry -> containerNames.contains(entry
            .getString("Name"));
      }
        break;
      case node:
      {
        String containerName = this.networkFailureTarget != null 
            ? "/repmgrpoc_" + this.networkFailureTarget + "_1" :  containers.stream()
            .sorted((o1, o2) -> random.nextInt())
            .findFirst().get()
            .getString("Name");
        System.out.println("Network isolation for node " + containerName.substring(1));
        filter = entry -> entry
            .getString("Name").equals(containerName);
      }
        break;
      case primary:
      {
        Optional<String> foundPrimary = parent.getMasterFromRepMgrThroughConsul();
        if (!foundPrimary.isPresent()) {
          System.out.println("Network isolation for primary skipped since primary was not found");
          return;
        }
        
        String primary = foundPrimary.get();
        
        System.out.println("Network isolation for primary at " + primary);
        filter = entry -> entry
            .getString("Name").equals("/repmgrpoc_" + primary + "_1");
      }
        break;
      default:
        throw new IllegalArgumentException("Invalid type " + networkFailureType);
    }
    
    String addresses = containers.stream()
        .filter(filter)
        .map(container -> container
            .getJsonObject("NetworkSettings")
            .getJsonObject("Networks")
            .getJsonObject("repmgrpoc_network")
            .getString("IPAddress"))
        .collect(Collectors.joining(","));
    
    parent.waitProcessAndGet("iptables -N NETWORK-FAILURE");
    parent.waitProcessAndGet("iptables -A NETWORK-FAILURE -s " + addresses + " -j RETURN");
    parent.waitProcessAndGet("iptables -A NETWORK-FAILURE -j DROP");
    parent.waitProcessAndGet("iptables -I DOCKER-USER -d " + addresses + " -j NETWORK-FAILURE");
    
    parent.interruptibleSleep(this.networkFailureDuration != null ? this.networkFailureDuration
        : networkFailureType.getSecondsToSleep(), TimeUnit.SECONDS);
    
    parent.waitProcessAndGet("iptables -D DOCKER-USER -d " + addresses + " -j NETWORK-FAILURE");
    parent.waitProcessAndGet("iptables -F NETWORK-FAILURE");
    parent.waitProcessAndGet("iptables -X NETWORK-FAILURE");
  }
}
