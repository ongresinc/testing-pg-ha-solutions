/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

public class TestcaseException extends RuntimeException {

  private static final long serialVersionUID = -1L;

  public TestcaseException(String message) {
    super(message);
  }

  public TestcaseException(Throwable cause) {
    super(cause);
  }

  public TestcaseException(String message, Throwable cause) {
    super(message, cause);
  }

  public TestcaseException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
