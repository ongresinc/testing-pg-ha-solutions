/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

import com.google.common.base.Charsets;
import com.google.common.net.HostAndPort;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.StringReader;
import java.lang.ProcessBuilder.Redirect;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Command(subcommands = {
    TestcaseRestartInstance.class,
    TestcaseNetworkFailure.class,
    TestcaseRandom.class,
    MonitorPrimaryInstance.class,
})
public class Main implements Runnable {

  @Option(names = {"-h", "--help"}, usageHelp = true,
      description = "Displays this help message and quits.")
  private boolean helpRequested = false;
  
  @Option(names = {"--repmgr-version"},
      description = "The repmgr version.")
  protected String repmgrVersion = "4.1.0";
  
  @Option(names = {"--consul-host"},
      description = "Consul host to use for API access.")
  protected String consulHost = "repmgrpoc_consul-1_1";
  
  public static void main(String[] args) throws Exception {
    CommandLine.run(new Main(), System.err, args);
  }

  public boolean isHelpRequested() {
    return helpRequested;
  }

  @Override
  public void run() {
    new CommandLine(new Main()).usage(System.out);
  }

  public static class SubCommand implements Runnable {
    @ParentCommand
    protected Main parent;
    
    @Override
    public void run() {
      new CommandLine(new Main()).usage(System.out);
    }
  }
  
  
  protected <T> T waitUntil(Supplier<T> untilState, Function<T, Boolean> untilCondition, 
      int timeoutDuration, TimeUnit unit) {
    Instant timeout = Instant.now().plusMillis(unit.toMillis(timeoutDuration));
    T state;
    while (!untilCondition.apply((state = untilState.get()))
        && Instant.now().isBefore(timeout)) {
      interruptibleSleep(100, TimeUnit.MILLISECONDS);
    }
    
    if (!Instant.now().isBefore(timeout)) {
      throw new TestcaseException(
          "Operation timed out befor condition was meet (state: " + state + ")");
    }
    
    return state;
  }

  protected void interruptibleSleep(int duration, TimeUnit unit) {
    try {
      Thread.sleep(unit.toMillis(duration));
    } catch (InterruptedException ex) {
      Thread.interrupted();
      throw new CompletionException(ex);
    }
  }
  
  protected Client buildClient() {
    return ClientBuilder
        .newClient();
  }

  protected int waitProcess(String command) {
    try {
      Process process = new ProcessBuilder(getProcessArguments(command))
          .redirectError(Redirect.PIPE)
          .redirectInput(Redirect.PIPE)
          .start();
      PipedOutputStream outOutputStream = new PipedOutputStream();
      PipedOutputStream errOutputStream = new PipedOutputStream();
      Consumer<String> outConsumer = line -> System.out.println(line);
      Consumer<String> errConsumer = line -> System.err.println(line);
      
      CompletableFuture<Void> output = CompletableFuture.allOf(
          pipedWriter(outOutputStream, outConsumer),
          pipedWriter(errOutputStream, errConsumer),
          pipedReader(process, outOutputStream, errOutputStream));
      
      process.waitFor();
      
      output.join();
      
      return process.exitValue();
    } catch (IOException | InterruptedException ex) {
      throw new TestcaseException(ex);
    }
  }
  
  protected String waitProcessAndGet(String command) {
    try {
      Process process = new ProcessBuilder(getProcessArguments(command))
          .redirectError(Redirect.PIPE)
          .redirectInput(Redirect.PIPE)
          .start();
      StringBuffer stringBuilder = new StringBuffer();
      PipedOutputStream outOutputStream = new PipedOutputStream();
      PipedOutputStream errOutputStream = new PipedOutputStream();
      String lineSeparator = System.getProperty("line.separator");
      Consumer<String> outConsumer = line -> {
        stringBuilder.append(line);
        stringBuilder.append(lineSeparator);
      };
      Consumer<String> errConsumer = line -> System.err.println(line);
      
      CompletableFuture<Void> output = CompletableFuture.allOf(
          pipedWriter(outOutputStream, outConsumer),
          pipedWriter(errOutputStream, errConsumer),
          pipedReader(process, outOutputStream, errOutputStream));
      
      if (process.waitFor() != 0) {
        throw new TestcaseException("Command '" + command 
            + "' exit with code " + process.exitValue());
      }
      
      output.join();
      
      return stringBuilder.toString();
    } catch (IOException | InterruptedException ex) {
      throw new TestcaseException(ex);
    }
  }

  private CompletableFuture<Void> pipedWriter(
      PipedOutputStream pipedOutputStream, Consumer<String> consumer) throws IOException {
    PipedInputStream pipeInputStream = new PipedInputStream(pipedOutputStream);
    return CompletableFuture.runAsync(() -> {
      try (
          Scanner out = new Scanner(pipeInputStream, Charsets.UTF_8.name());
          ) {
        while (out.hasNextLine()) {
          if (out.hasNextLine()) {
            consumer.accept(out.nextLine());
          }
        }
      }
    });
  }

  private CompletableFuture<Void> pipedReader(Process process, PipedOutputStream outOutputStream,
      PipedOutputStream errOutputStream) {
    return CompletableFuture.runAsync(() -> {
      try {
        while (process.isAlive() 
            || process.getInputStream().available() > 0
            || process.getErrorStream().available() > 0) {
          if (process.getInputStream().available() > 0) {
            int read = process.getInputStream().read();
            if (read != -1) {
              outOutputStream.write(read);
            }
          }
          if (process.getErrorStream().available() > 0) {
            int read = process.getErrorStream().read();
            if (read != -1) {
              errOutputStream.write(read);
            }
          }
        }
      } catch (Exception ex) {
        System.err.println(ex);
      } finally {
        try {
          outOutputStream.close();
        } catch (Throwable ex) {
          System.err.println(ex);
        }
        try {
          errOutputStream.close();
        } catch (Throwable ex) {
          System.err.println(ex);
        }
      }
    });
  }

  private List<String> getProcessArguments(String command) {
    List<String> commandArgs = new ArrayList<>();
    boolean quoted = false;
    for (String commandChunk : command
        .split(" +")) {
      boolean quoting = quoted;
      
      while (commandChunk.contains("'")) {
        quoted = !quoted;
        commandChunk = commandChunk.substring(0, commandChunk.indexOf("'"))
            + commandChunk.substring(commandChunk.indexOf("'") + 1, commandChunk.length());
      }
      
      if (quoting) {
        commandArgs.set(commandArgs.size() - 1,
            commandArgs.get(commandArgs.size() - 1) + " " + commandChunk);
      } else {
        commandArgs.add(commandChunk);
      }
    }
    return commandArgs;
  }
  
  protected String abbreviatedType(String type) {
    switch (type) {
      case "postgresql":
        return "postgres";
      case "mysql":
        return "mysql";
      default:
        throw new RuntimeException("Service type " + type + " is unknown");
    }
  }

  public static class HostAndPortConverter implements ITypeConverter<HostAndPort> {
    @Override
    public HostAndPort convert(String value) throws Exception {
      return HostAndPort.fromString(value);
    }
  }
  
  protected Set<String> getDnsAddresses(String hostname, List<HostAndPort> dnsHostAndPorts) {
    return dnsHostAndPorts
        .stream()
        .flatMap(dnsHostAndPort -> getDnsAddresses(hostname, dnsHostAndPort).stream())
        .collect(Collectors.toSet());
  }
  
  protected Set<String> getDnsAddresses(String hostname, HostAndPort dnsHostAndPort) {
    try {
      Resolver dnsResolver = new SimpleResolver(dnsHostAndPort.getHost());
      dnsResolver.setPort(dnsHostAndPort.getPort());
      Name primaryName = Name.fromString(hostname, Name.root); 
      Message query = Message.newQuery(Record.newRecord(
          primaryName, Type.A, DClass.IN));
      Message response = dnsResolver.send(query);
      return Arrays.asList(response.getSectionArray(Section.ANSWER))
        .stream()
        .map(r -> ((ARecord) r).getAddress().getHostAddress())
        .collect(Collectors.toSet());
    } catch (IOException ex) {
      return new HashSet<>(0);
    }
  }

  protected Optional<String> getMasterFromRepMgrThroughConsul() {
    Set<String> foundMasters;
    try {
      Optional<String> consulAddress = Json.createReader(new StringReader(
          waitProcessAndGet("docker inspect " + consulHost)))
          .read()
          .asJsonArray()
          .stream()
          .map(entry -> entry.asJsonObject())
          .map(entry -> entry
              .getJsonObject("NetworkSettings")
              .getJsonObject("Networks")
              .getJsonObject("repmgrpoc_network")
              .getString("IPAddress"))
          .findFirst();
      
      if (!consulAddress.isPresent()) {
        return Optional.empty();
      }
      
      foundMasters = Json.createReader(new StringReader(buildClient()
          .target("http://" + consulAddress.get() + ":8500/v1/health/service/postgresql")
          .request()
          .get(String.class)))
          .read()
          .asJsonArray()
          .stream()
          .map(node -> node.asJsonObject())
          .filter(node -> node.get("Checks")
              .asJsonArray()
              .stream()
              .map(check -> check.asJsonObject())
              .filter(check -> check
                  .getString("CheckID").equals("serfHealth"))
              .allMatch(check -> check
                  .getString("Status").equals("passing")))
          .filter(node -> node.get("Checks")
              .asJsonArray()
              .stream()
              .map(check -> check.asJsonObject())
              .filter(check -> check
                  .getString("CheckID").equals("service:postgresql"))
              .allMatch(check -> check
                  .getString("Status").equals("passing")))
          .map(node -> node.get("Node").asJsonObject().getString("Node"))
          .collect(Collectors.toSet());
    } catch (Throwable ex) {
      ex.printStackTrace(System.err);
      return Optional.empty();
    }
    
    if (foundMasters.size() == 1) {
      return Optional.of(foundMasters.iterator().next());
    }
    
    System.err.println("Found " + foundMasters.size() + " masters: " + foundMasters);
    return Optional.empty();
  }

  protected String getMasterQueryByVersion() {
    switch (repmgrVersion) {
      case "3.3":
        return "SELECT name FROM repmgr.repl_nodes WHERE type='master' AND active != 'f'";
      default:
        return "SELECT node_name FROM repmgr.nodes WHERE type='primary' AND active != 'f'";
    }
  }
  
}
