/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

public class TestcasePostconditionsException extends RuntimeException {

  private static final long serialVersionUID = -1L;

  public TestcasePostconditionsException(String message) {
    super(message);
  }

  public TestcasePostconditionsException(Throwable cause) {
    super(cause);
  }

  public TestcasePostconditionsException(String message, Throwable cause) {
    super(message, cause);
  }

  public TestcasePostconditionsException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
