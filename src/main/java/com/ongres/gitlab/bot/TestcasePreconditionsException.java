/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

public class TestcasePreconditionsException extends RuntimeException {

  private static final long serialVersionUID = -1L;

  public TestcasePreconditionsException(String message) {
    super(message);
  }

  public TestcasePreconditionsException(Throwable cause) {
    super(cause);
  }

  public TestcasePreconditionsException(String message, Throwable cause) {
    super(message, cause);
  }

  public TestcasePreconditionsException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
