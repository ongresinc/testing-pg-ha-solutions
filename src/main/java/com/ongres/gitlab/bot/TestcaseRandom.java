/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

import com.google.common.collect.ImmutableList;
import com.ongres.gitlab.bot.Main.SubCommand;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.List;
import java.util.Random;

@Command(name = "testcase-random", 
    description = "Run a random test case")
public class TestcaseRandom extends SubCommand {
  
  @Option(names = {"--exclude-testcase"},
      description = "A test case to exclude (you can specify multiples).")
  protected List<String> excludedTescases = ImmutableList.of();
  
  @Override
  public void run() {
    ImmutableList<String> testcases = ImmutableList.of(
        "testcase-failover", 
        "testcase-network-failure",
        "testcase-restart-instance");
    Random random = new Random();
    String randomTestcase = testcases
        .stream()
        .filter(testcase -> !excludedTescases.contains(testcase))
        .sorted((o1,o2) -> random.nextInt())
        .findFirst()
        .get();
    CommandLine.run(new Main(), System.err, randomTestcase);
  }
}
